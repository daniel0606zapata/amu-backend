# Amu-Backend

# **¡Hola a todos! Bienvenidos** <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px">

El proyecto se propone como solución a la necesidad identificada en los laboratorios de la Universidad de Medellín.

## Autores ✒️


| [<img src="https://avatars.githubusercontent.com/u/53974843?v=4" width=115><br><sub>David Medina</sub>](https://github.com/DavidMedinaO) | [<img src="https://avatars.githubusercontent.com/u/60439694?s=400&u=c9adea273f4f009648d2f8a116eb4bb3c60c8d77&v=4" width=115><br><sub>Santiago forero</sub>](https://github.com/forero-kun) |  [<img src="https://avatars.githubusercontent.com/u/61246635?v=4" width=115><br><sub>Daniel Atehortua</sub>](https://github.com/Jatez) |   [<img src="https://avatars.githubusercontent.com/u/61894194?v=4" width=115><br><sub>Harrison velez</sub>](https://github.com/HarryKill2001) |
| :---: | :---: | :---: | :---: |

## **Tecnologías**

### Frontend

![HTML](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)
![CSS](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)
![JS](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)
![VUE](https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D)



### Backend

![NODE](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)
![EXPRESS](https://img.shields.io/badge/Express.js-404D59?style=for-the-badge)

### Bases de datos

![MONGODB](https://img.shields.io/badge/MongoDB-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white)

## **Diseño de Casos de Prueba**:
- [🔭 Diseño](https://docs.google.com/spreadsheets/d/1xnsxrPEcx1vGdrFHuL2r-OuPf9q6fyJmAvupIG_XANY/edit?usp=sharing)

## **Pruebas**:
- [Pruebas](https://drive.google.com/file/d/1Sr9G6vMWE73vEnuoM7WqQVxbEild2aEn/view?usp=sharing)

## **Referencias**:
- [🔧 Documentacion Mongobd - Node](https://www.freecodecamp.org/news/build-a-restful-api-using-node-express-and-mongodb/)
- [🚀 Documentacion Pruebas](https://www.paradigmadigital.com/dev/testeando-javascript-mocha-chai/)







